package main

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

var (
	//go:embed html/last.html
	lastHTML string
)

type Server struct {
	db *DB
}

func (s *Server) lastHandler(w http.ResponseWriter, r *http.Request) {
	lastText := "No entries"

	entry, err := s.db.Last()
	if err == nil {
		time := entry.Time.Format("2006-01-02T15:04")
		lastText = fmt.Sprintf("[%s] %s by %s", time, entry.Content, entry.User)
	}
	fmt.Fprintf(w, lastHTML, lastText)
}

func (s *Server) newHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var e Entry

	if err := json.NewDecoder(r.Body).Decode(&e); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	if e.User == "" || e.Content == "" {
		http.Error(w, "missing data", http.StatusBadRequest)
	}

	e.Time = time.Now()
	if err := s.db.Add(e); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	json.NewEncoder(w).Encode(e)
}

func (s *Server) Health() error {
	return s.db.Health()
}

func (s *Server) healthHandler(w http.ResponseWriter, r *http.Request) {
	if err := s.Health(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "OK\n")
}

func main() {
	var err error
	db, err := NewDB("host=localhost user=postgres password=s3cr3t sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	s := Server{db}

	r := mux.NewRouter()
	r.HandleFunc("/last", s.lastHandler).Methods("GET")
	r.HandleFunc("/new", s.newHandler).Methods("POST")
	r.HandleFunc("/health", s.healthHandler).Methods("GET")

	http.Handle("/", r)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
