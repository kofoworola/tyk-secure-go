#!/bin/bash

docker run \
    -d \
    -e POSTGRES_PASSWORD=s3cr3t \
    -p 5432:5432 \
    --name gosec-pg \
    -v $PWD/sql/schema:/docker-entrypoint-initdb.d \
    postgres:15-alpine
